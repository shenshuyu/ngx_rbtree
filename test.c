#include <stdio.h>
#include <stdlib.h>
#include "ngx_rbtree.h"


int main()
{
    printf("hello nginx!\n");
    ngx_rbtree_t tree;
    ngx_rbtree_node_t sentinel;
    ngx_rbtree_init(&tree,&sentinel,ngx_rbtree_insert_value);
    
    for(int i = 99; i >= 0 ; i--)
    {
        ngx_rbtree_node_t *rbnode = (ngx_rbtree_node_t *)malloc(sizeof(ngx_rbtree_node_t));
        rbnode->key = 1008 - i;
        rbnode->parent = NULL;
        rbnode->left = NULL;
        rbnode->right = NULL;
        ngx_rbtree_insert(&tree, rbnode);
    }

    for(int i = 0; i < 100; i++)
    {
        ngx_rbtree_node_t *p = ngx_rbtree_min(tree.root,&sentinel);
        printf(">>>key:%d \n", p->key);
        ngx_rbtree_delete(&tree,p);
        free(p);
    }
    return 0;
}
